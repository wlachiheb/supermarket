package offers;

/**
 * Created by MyPc on 28/11/2019.
 */
public class PriceWithoutOffers implements PricingService{
    @Override
    public double pricingByType(Product product){
        return PricingService.super.defaultPrice(product);
    }
}
