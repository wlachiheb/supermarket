package offers;

/**
 * Created by MyPc on 28/11/2019.
 */
public interface PricingService {
    public double pricingByType(Product product);

    public default double defaultPrice(Product product){
        double price = 0.0;
        if(product.getName().equals(ProductName.Orange.getName())){
            price = product.getQuantity() * product.getPrice();
        }
        return price;
    }
}
