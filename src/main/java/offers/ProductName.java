package offers;

/**
 * Created by MyPc on 28/11/2019.
 */
public enum ProductName {
    Apple("Apple"),
    Orange("Orange"),
    Watermelon("Watermelon");

    private String name;

     ProductName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
