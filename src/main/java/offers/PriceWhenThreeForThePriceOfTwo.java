package offers;

/**
 * Created by MyPc on 28/11/2019.
 */
public class PriceWhenThreeForThePriceOfTwo implements PricingService {
    @Override
    public double pricingByType(Product product){
        double price = 0.0;
        if(product.getQuantity() % 3 == 0){
            price = (product.getQuantity() / 3) * 2 * product.getPrice();
        } else {
            int rest = product.getQuantity() % 3;
            price = (((product.getQuantity() / 3) * 2) + rest) * product.getPrice();
        }
        return price;
    }
}
