package offers;

/**
 * Created by MyPc on 28/11/2019.
 */
public class PriceWhenBuyOneThenGetOneFree implements PricingService {

    @Override
    public double pricingByType(Product product){
        double price = 0.0;
        if(product.getQuantity() % 2 == 0){
            price = product.getQuantity() * product.getPrice() / 2 ;
        } else {
            price = ((product.getQuantity() / 2) + 1) * product.getPrice();
        }
        return price;
    }
}
