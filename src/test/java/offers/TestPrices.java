package offers;

/**
 * Created by MyPc on 29/11/2019.
 */

import org.junit.Assert;
import org.junit.Test;

public class TestPrices {
    @Test
    public void testPriceForOrange() {
        Product orange = new Product(ProductName.Orange.getName(), 0.5 , 3 );
        PriceWithoutOffers pwo = new PriceWithoutOffers();
        Assert.assertEquals(1.5, pwo.defaultPrice(orange), 0);
    }


    @Test
    public void testPriceForApple() {
        Product apple = new Product(ProductName.Apple.getName(), 0.2 , 4 );
        PriceWhenBuyOneThenGetOneFree pwo = new PriceWhenBuyOneThenGetOneFree();
        Assert.assertEquals(0.4, pwo.pricingByType(apple), 0);
    }

    @Test
    public void testPriceForWatermelon() {
        Product watermelon = new Product(ProductName.Watermelon.getName(), 0.8 , 5 );
        PriceWhenThreeForThePriceOfTwo pwo = new PriceWhenThreeForThePriceOfTwo();
        Assert.assertEquals(3.2, pwo.pricingByType(watermelon ), 0);
    }
}




